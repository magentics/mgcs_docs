Mgcs_Docs: Documentation skeleton module for Magento 1
======================================================

Introduction
------------
This is a skeleton for adding your own documentation to the backend of your projects and modules. It adds a menu
item and adds Markdown rendering capabilities.


Compatibility
-------------
I have only tested this extension in Magento CE 1.9. Most probably it will work just fine on earlier versions (like CE 1.6+).


Installation
------------
Use the excellent [modman tool](https://github.com/colinmollenhour/modman).

Adding documentation to your own project or module
--------------------------------------------------
To add documentation to your project, I suggest you create a separate module for this and name it like *Projectname*_Docs.
If you already have a module you want to add documentation to, use the following steps. In this steps, I assume your
module is named Project_Docs. You need to change this to your own module name.

#### Add the menu item
Edit or add `adminhtml.xml` and add the following:

    :::xml
    <?xml version="1.0"?>
    <config>
        <menu>
            <docs>
                <children>
                    <!-- you can name this node anything you want - it's not used -->
                    <project_docs translate="title" module="project_docs">
                        <title>Project Name</title>
                        <sort_order>10</sort_order>
                        <!-- use the page id here - see config.xml -->
                        <action>adminhtml/docs/view/page/project_docs</action>
                    </project_docs_index>
                </children>
            </docs>
        </menu>
    </config>

#### Add the page
Edit your `config.xml` and add the following:

    :::xml
    <global>
        <mgcs_docs>
            <pages>
                <!-- the name of this node is used as page id in the url - see also adminhtml.xml -->
                <project_docs>
                    <module>Project_Docs</module>
                    <file>index.md</file>
                    <title>Project Name Docs</title>
                </project_docs>
            </pages>
        </mgcs_docs>
    </global>

#### Add your documentation pages

For now, only Markdown is supported. You can add them in a folder named `/docs` in your module. That's
where the module will look for the files referenced in your `config.xml`.

#### See your documentation!

![screenshot.png](https://bitbucket.org/repo/6BGrqd/images/937295608-screenshot.png)


Future
------
- Add more file formats, like html

<?php

class Mgcs_Docs_Adminhtml_DocsController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('docs');
    }

    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('docs')
            ->_addBreadcrumb($this->__('Docs'), $this->__('Docs'))
        ;
        return $this;
    }


    /**
     * Index action
     */
    public function viewAction()
    {
        $this->_title(Mage::helper('cms')->__('Docs'));
        $this->_initAction();

        $page  = $this->getRequest()->getParam('page');
        $pages = Mage::helper('mgcs_docs')->getPages();
        if (!isset($pages[$page])) {
            return $this->_error($this->__('Page does not exist'));
        }

        $this->_title($pages[$page]->getTitle());

        Mage::register('docs_page', $pages[$page]);

        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _error($message)
    {
        echo $message;
    }

}

<?php

class Mgcs_Docs_Block_Adminhtml_Doc extends Mage_Adminhtml_Block_Template
{

    /**
     * Get the rendered markdown
     *
     * @return string
     */
    public function getRenderedContent()
    {
        return Mage::helper('mgcs_docs')->renderPage($this->getPage());
    }

    /**
     * Get the current viewed page
     *
     * @return Varien_Object
     */
    public function getPage()
    {
        return Mage::registry('docs_page');
    }

}
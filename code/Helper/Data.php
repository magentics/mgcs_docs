<?php

class Mgcs_Docs_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_PAGES = 'global/mgcs_docs/pages';

    /**
     * Get the available documentation pages
     *
     * @return array  Array of Varien_Objects with keys 'module', 'title', 'file', ...
     */
    public function getPages()
    {
        $node = Mage::getConfig()->getNode(self::XML_PATH_PAGES);
        $pages = array();
        foreach ($node->children() as $code => $pageData) {
            $pageData = (array)$pageData;
            $pageData['code'] = $code;
            $pages[$code] = new Varien_Object($pageData);
        }
        return $pages;
    }

    public function renderPage(Varien_Object $page)
    {
        $file = $this->getFullPath($page);

        $contents = file_get_contents($file);
        $output   = '';

        $type = $page->getType();
        if (!$type) {
            $type = pathinfo($file, PATHINFO_EXTENSION);
        }
        switch (strtolower($type)) {
            case 'md':
            case 'markdown':
                require_once Mage::getModuleDir('', 'Mgcs_Docs') . '/lib/Parsedown.php';
                $parser = new Parsedown();
                $output = $parser->text($contents);
                break;
            default:
                $output = $contents;
        }

        return $output;
    }

    /**
     * Get the full path to a page
     *
     * @throws Mage_Core_Exception  If the file does not exist
     * @param Varien_Object $page
     * @return string
     */
    public function getFullPath(Varien_Object $page)
    {
        $dir = Mage::getModuleDir('', $page->getModule()) . '/docs';
        if (!is_dir($dir)) {
            Mage::throwException($this->__('Docs directory does not exist'));
        }
        $file = $dir . '/' . preg_replace('/\.+/', '.', $page->getFile());
        if (!file_exists($file)) {
            Mage::throwException($this->__('Doc source page does not exist'));
        }
        return $file;
    }

}